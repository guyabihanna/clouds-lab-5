
document.getElementById('dashboard').style.display = "none"

document.getElementById('login').addEventListener('click', GoogleLogin)

document.getElementById('logout').addEventListener('click', LogoutUser)



var firebaseConfig = {
    apiKey: "AIzaSyCmHM1Sze_5Sq-7dKGQmcNCdudOA91YWbQ",
    authDomain: "clouds-lab-5.firebaseapp.com",
    projectId: "clouds-lab-5",
    storageBucket: "clouds-lab-5.appspot.com",
    messagingSenderId: "572011464001",
    appId: "1:572011464001:web:afd956c854689f22420dc0"
};

firebase.initializeApp(firebaseConfig);


let provider = new firebase.auth.GoogleAuthProvider()


var user_data;

function GoogleLogin() {
    console.log('Login Btn Call')
    firebase.auth().signInWithPopup(provider).then(res => {
        console.log(res.user)
        user_data = res.user
        document.getElementById('LoginScreen').style.display = "none"
        document.getElementById('dashboard').style.display = "block"
        showUserDetails(res.user)
    }).catch(e => {
        console.log(e)
    })
}

function showUserDetails(user) {
    
    sessionStorage.setItem("user",user.displayName)
    location.href = "/dashboard.html"
    
}

function checkAuthState() {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            document.getElementById('LoginScreen').style.display = "none"
            document.getElementById('dashboard').style.display = "block"
            showUserDetails(user)
        } else {

        }
    })
}

function LogoutUser() {
    console.log('Logout Btn Call')
    firebase.auth().signOut().then(() => {
        document.getElementById('LoginScreen').style.display = "block"
        document.getElementById('dashboard').style.display = "none"
    }).catch(e => {
        console.log(e)
    })
}

checkAuthState()

