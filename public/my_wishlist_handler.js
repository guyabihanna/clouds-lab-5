// Your web app's Firebase configuration
var firebaseConfig = {
    // Paste your Config Code
    apiKey: "AIzaSyCmHM1Sze_5Sq-7dKGQmcNCdudOA91YWbQ",
    authDomain: "clouds-lab-5.firebaseapp.com",
    projectId: "clouds-lab-5",
    storageBucket: "clouds-lab-5.appspot.com",
    // databaseURL: "https://clouds-lab-5-default-rtdb.firebaseio.com/",
    // databaseURL: "",
    messagingSenderId: "572011464001",
    appId: "1:572011464001:web:afd956c854689f22420dc0"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let provider = new firebase.auth.GoogleAuthProvider()


// db.settings({
//     timestamps
// })

var user;
var user_movie_ids = [];

function LogoutUser() {
    console.log('Logout Btn Call')
    firebase.auth().signOut().then(() => {
        location.href = "/index.html"
    }).catch(e => {
        console.log(e)
    })
}

function checkAuthState() {
    firebase.auth().onAuthStateChanged(usr => {
        user = usr
        document.getElementById('username-btn').innerHTML = user.displayName
    })
}

function checkDBRecord() {
    const db = firebase.firestore()

    db.collection('users').get().then((snapshot) => {
        let IDExists = 0
        for (let i = 0; i < snapshot.docs.length; i++) {
            let doc_id = snapshot.docs[i].id
            let doc_data = snapshot.docs[i].data()
            let usr_email = user.email
            if (usr_email == doc_id) {
                IDExists = 1
                console.log("Existing user")
                console.log(doc_data['movie_ids'])
                user_movie_ids = doc_data['movie_ids']

            }
        }
        if (IDExists == 0) {
            db.collection('users').doc(user.email).set({
                movie_ids: [],
            });
        }

        CreateWishList()

    })
}

function Init() {


    checkAuthState()
    document.getElementById('wishlist-btn').addEventListener('click', OpenWishList)
    document.getElementById('logout-btn').addEventListener('click', LogoutUser)
    document.getElementById('menu-btn').addEventListener('click', ReturnToMenu)

    checkDBRecord()
    console.log(user_movie_ids)


}


function CreateWishList() {
    firebase.database().ref('movies-list').once('value', function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
            var node = document.createElement("Button");
            node.innerHTML = "Title: " + childData.title + " | Year: " + childData.year + " | Genre: " + childData.genre + " " + "❤️"
            node.id = childData.id
            node.addEventListener('click', () => {
                RemoveFromWishList(node.id)
            })
            if (user_movie_ids.includes(node.id)) {
                document.getElementById('movies').appendChild(node)
            } else {

            }
        });
    });
}

function RemoveFromWishList(ID) {
    const db = firebase.firestore()
    db.collection('users').get(user.email).then((snapshot) => {
        let old_ids = snapshot.docs[0].data()['movie_ids']
        old_ids = old_ids.filter(item => item !== ID)
        console.log(old_ids)
        db.collection('users').doc(user.email).set({
            movie_ids: old_ids
        }).then(() => {
            console.log("success")
            window.location.reload()
        });

    })


}

function OpenWishList() {
    console.log("Opening Wishlist")
}

function ReturnToMenu(){
    location.href = "/dashboard.html"
}

Init()



